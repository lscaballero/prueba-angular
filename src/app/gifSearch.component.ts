import { Component } from '@angular/core';
import { gifService } from './gifService';

export class gifSearch {
  query : string = '';
  gif : any[] = [];

  constructor(private gifService: gifService) {}

  search(): void {
    this.gifService.searchGifs(this.query).subscribe((gif) => {
      this.gif = gif;
    });
  }
}
