import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Gif} from './gifModel';


export class gifService {
  private apiKey = 'EyLXdUUJZqQhguJVmwq97SUpNwo9S67q';
  private apiUrl = 'https://api.giphy.com/v1/gifs/search';

  constructor(private http: HttpClient) {}

  searchGifs(query: string): Observable <Gif[]> {
    const url = `${this.apiUrl}?api_key=${this.apiKey}q&q=${query} }`;
    return this.http.get<Gif[]>(url);
  }
}
